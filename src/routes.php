<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    try{
        $p = $request->getQueryParams();
        if(empty($p['filtr'])){
            //nezadal nic - vsichni
            $stmt = $this->db->query('SELECT * FROM person');
        } else {
             //filtr
            $stmt = $this->db->prepare(
                    'SELECT * FROM person WHERE first_name LIKE :f OR last_name LIKE :f ORDER BY last_name'
                    );
            $stmt->bindValue(':f','%'.$p['filtr'].'%');
            $stmt->execute();
        }
        
 $data = $stmt->fetchAll();
 $this->view->render($response, 'index.latte',[
     'osoby' => $data
 ]);
    } catch (Exception $ex) {
       $this->logger->error($ex->getMessage());
       exit('Chyba DB, zkuste pozdeji');
      //  exit($ex->getMessage());
    }
})->setName('redir');
